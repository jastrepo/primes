#include <iostream>
#include <vector>
#include <png++/png.hpp>
#include <chrono>
#include <cmath>

using namespace std;

template<typename U>
vector<U> getPrimes(size_t limit) {
  vector<bool> is_prime(limit);
  size_t x2 = 0, y2, i = 1, j, n, rootOfLimit = sqrt(limit) + 1;
  is_prime[2] = is_prime[3] = true;
  for (; i < rootOfLimit; ++i) {
    x2 += 2 * i - 1;
    y2 = 0;
    for (j = 1; j < rootOfLimit; ++j) {
      y2 += 2 * j - 1;
      n = 4 * x2 + y2;
      if (n < limit && (n % 12 == 1 || n % 12 == 5)) {
        is_prime[n] = !is_prime[n];
      }
      n -= x2;
      if (n < limit && n % 12 == 7) {
        is_prime[n] = !is_prime[n];
      }
      n -= 2 * y2;
      if (i > j && n < limit && n % 12 == 11) {
        is_prime[n] = !is_prime[n];
      }
    }
  }
  for (i = 5; i < rootOfLimit; ++i) {
    if (is_prime[i]) {
      n = i * i;
      for (j = n; j < limit; j += n) {
        is_prime[j] = false;
      }
    }
  }
  vector<U> primes(limit);
  primes[0] = 1;
  primes[1] = 2;
  primes[2] = 3;
  primes[3] = 5;
  size_t primesNum = 3;
  for (i = 6; i <= limit; ++i) {
    if ((is_prime[i]) && (i % 3 != 0) && (i % 5 != 0))
      primes[++primesNum] = i;
  }
  primes.resize(primesNum + 1);
  return primes;
}

template<typename U>
vector<U> getFi(size_t limit) {
  const vector<U> &primes = getPrimes<U>(limit + 1);

  vector<U> fi(limit);

  size_t numOfPrimes = primes.size() - 1, numOfFactors = log(limit) / log(2);
  vector<size_t> primeFactors(numOfFactors);
  primeFactors[numOfFactors - 1] = 1;
  size_t firstNonUnitPrime = numOfFactors - 1, primeFactorToChange = numOfFactors - 1;
  U fiArgument, lastPrime, samePrimesProduct, fiValue, currentPrime;
  while (true) {
    {
      fiValue = fiArgument = samePrimesProduct = 1;
      lastPrime = primes[primeFactors[firstNonUnitPrime]];
      for (size_t i = firstNonUnitPrime + 1; i < numOfFactors; ++i) {
        currentPrime = primes[primeFactors[i]];
        if (currentPrime != lastPrime) {
          fiArgument *= samePrimesProduct * lastPrime;
          fiValue *= samePrimesProduct * lastPrime - samePrimesProduct;

          lastPrime = currentPrime;
          samePrimesProduct = 1;
        } else {
          samePrimesProduct *= currentPrime;
        }
      }
      fiArgument *= samePrimesProduct * lastPrime;
      fiValue *= samePrimesProduct * lastPrime - samePrimesProduct;
    } // Calculate fiArgument and fiValue

    {
      if (0 < fiArgument && fiArgument <= limit) {
        fi[fiArgument - 1] = fiValue;
        primeFactorToChange = numOfFactors - 2;
      } else {
        primeFactors[numOfFactors - 1] = numOfPrimes;
      }
    } // Set fi from fiArgument to fiValue

    {
      if (primeFactors[numOfFactors - 1] > numOfPrimes - 1) {
        if (firstNonUnitPrime == 0) {
          break;
        }
        if (primeFactorToChange >= 0) {
          firstNonUnitPrime = min(firstNonUnitPrime, primeFactorToChange);
          primeFactors[primeFactorToChange]++;
          for (size_t i = primeFactorToChange + 1; i < numOfFactors; ++i) {
            primeFactors[i] = primeFactors[i - 1];
          }
          primeFactorToChange--;
        }
      } else {
        primeFactors[numOfFactors - 1]++;
      }
    } // Change primes vector
  }
  return fi;
}

template<typename U, typename F, typename D>
vector<U> getWrongFi(size_t limit) {
  const vector<U> &primes = getPrimes<U>(limit + 1);

  vector<U> fi(limit);

  size_t numOfPrimes = primes.size() - 1, numOfFactors = log(limit) / log(2);
  vector<size_t> primeFactors(numOfFactors);
  size_t firstNonUnitPrime = numOfFactors - 1, primeFactorToChange = numOfFactors - 1;
  U fiArgument, lastPrime, currentPrime;
  F fiValue;
  while (true) {
    {
      fiValue = fiArgument = lastPrime = 1;
      for (size_t i = firstNonUnitPrime; i < numOfFactors; ++i) {
        currentPrime = primes[primeFactors[i]];
        fiArgument *= currentPrime;
        if (currentPrime != lastPrime) {
          fiValue *= D(1) - D(1) / D(currentPrime);
          lastPrime = currentPrime;
        }
      }
      fiValue *= fiArgument;
    } // Calculate fiArgument and fiValue

    {
      if (0 < fiArgument && fiArgument <= limit) {
        fi[fiArgument - 1] = fiValue;
        primeFactorToChange = numOfFactors - 2;
      } else {
        primeFactors[numOfFactors - 1] = numOfPrimes;
      }
    } // Set fi from fiArgument to fiValue

    {
      if (primeFactors[numOfFactors - 1] > numOfPrimes - 1) {
        if (firstNonUnitPrime == 0) {
          break;
        }
        if (primeFactorToChange >= 0) {
          firstNonUnitPrime = min(firstNonUnitPrime, primeFactorToChange);
          primeFactors[primeFactorToChange]++;
          for (size_t i = primeFactorToChange + 1; i < numOfFactors; ++i) {
            primeFactors[i] = primeFactors[i - 1];
          }
          primeFactorToChange--;
        }
      } else {
        primeFactors[numOfFactors - 1]++;
      }
    } // Change primes vector
  }
  return fi;
}

template<typename T, typename U>
vector<U> getFibonacci(size_t limit) {
  vector<T> fibonacci(limit, 1);
  for (size_t i = 2; i < limit; ++i) {
    fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
  }
  return {fibonacci.begin(), fibonacci.end()};
}

template<typename U>
void makeNextRow(vector<U> &row, size_t end) {
  for (size_t i = 0; i < end - 1; ++i) {
    if (row[i] > row[i + 1]) {
      row[i] = row[i] - row[i + 1];
    } else {
      row[i] = row[i + 1] - row[i];
    }
  }
}

template<typename U>
void writeImage(const vector<U> &sequence, const string &name) {
  size_t limit = sequence.size();
  png::image<png::rgb_pixel> image(limit * 2, limit * 2);
  auto sequenceCopy = sequence;
  U val;
  size_t j, pos, end;
  png::rgb_pixel pixel;
  for (size_t i = 0; i < limit; ++i) {
    end = limit * 2 - i;
    pos = 0;
    for (j = i; j < end; j += 2) {
      val = sequenceCopy[pos];
      ++pos;
      if (val == 0) {
        pixel = {0, 255, 255};
      } else if (val == 1) {
        pixel = {255, 0, 255};
      } else if (val == 2) {
        pixel = {255, 255, 0};
      } else if (val == 3) {
        pixel = {255, 0, 0};
      } else if (val == 4) {
        pixel = {0, 255, 0};
      } else if (val == 5) {
        pixel = {0, 0, 255};
      } else {
        pixel = {255, 255, 255};
      }
      image[2 * i][j] = image[2 * i + 1][j] = image[2 * i][j + 1] = image[2 * i + 1][j + 1] = pixel;
    }
    makeNextRow<U>(sequenceCopy, limit - i);
  }
  image.write(name);
}

int main() {
  auto res = getPrimes<int>(1000);
  cout << res.size();
  for(int i = 0; i< 500; ++i){
    if(res[i] > 1000 || res[i] < 2){
      break;
    }
    cout << res[i] << ",";
  }
  /*size_t limit = 10000;
  auto primes = getPrimes<unsigned int>(limit * 2. * log(limit));
  primes.resize(limit);
  writeImage<unsigned int>(primes, "primes.png");
  writeImage(getFi<unsigned int>(limit), "fi.png");
  writeImage(getWrongFi<unsigned int, float, float>(limit), "wrongFi(float division).png");
  writeImage(getWrongFi<unsigned int, double, double>(limit), "wrongFi(double division).png");
  writeImage(getWrongFi<unsigned int, float, double>(limit), "wrongFi(float to double division).png");
  writeImage(getFibonacci<int, int>(limit), "wrongFibonacci(int to int overflow).png");
  writeImage(getFibonacci<int, unsigned int>(limit), "wrongFibonacci(int to unsigned int overflow).png");
  writeImage(getFibonacci<int, long long int>(limit), "wrongFibonacci(int to long long int overflow).png");*/
  return 0;
}
